package com.rencredit.jschool.boruak.taskmanager.listener.user.auth;

import com.rencredit.jschool.boruak.taskmanager.endpoint.AuthEndpoint;
import com.rencredit.jschool.boruak.taskmanager.endpoint.DeniedAccessException_Exception;
import com.rencredit.jschool.boruak.taskmanager.endpoint.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class UserLogOutListener extends AbstractListener {

    @Autowired
    private AuthEndpoint authEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout system.";
    }

    @Override
    @EventListener(condition = "@userLogOutListener.name() == #event.command")
    public void handle(final ConsoleEvent event) throws DeniedAccessException_Exception {
        @NotNull final SessionDTO session = systemObjectService.getSession();
        authEndpoint.logOut(session);
        systemObjectService.removeSession();
        System.out.println("Have been logout");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
