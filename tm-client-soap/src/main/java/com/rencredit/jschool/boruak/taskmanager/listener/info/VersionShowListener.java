package com.rencredit.jschool.boruak.taskmanager.listener.info;

import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import com.rencredit.jschool.boruak.taskmanager.service.InfoService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class VersionShowListener extends AbstractListener {

    @Autowired
    private InfoService infoService;

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version info.";
    }

    @Override
    @EventListener(condition = "@versionShowListener.name() == #event.command")
    public void handle(final ConsoleEvent event){
        System.out.println("[VERSION]");
        System.out.println(infoService.getVersion());
    }

}
