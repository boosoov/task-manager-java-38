package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class NotExistUsersListException extends AbstractException {

    public NotExistUsersListException() {
        super("Error! Users list is not exist...");
    }

}
