package com.rencredit.jschool.boruak.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IEntityManagerService {

    @NotNull
    EntityManager getEntityManager();

    void init();

}

