package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IProjectRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    @PostMapping
    public void create(@RequestBody ProjectDTO project) throws EmptyProjectException, EmptyUserIdException, EmptyLoginException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        projectService.create(user.getId(), project);
    }

    @Override
    @Nullable
    @GetMapping("/${id}")
    public ProjectDTO findOneByIdDTO(@PathVariable("id") String id) throws EmptyIdException, EmptyUserIdException, EmptyLoginException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        return projectService.findOneByIdDTO(user.getId(), id);
    }

    @Override
    @GetMapping("/exist/${id}")
    public boolean existsById(@PathVariable("id") String id) {
        return projectService.existsById(id);
    }

    @Override
    @Transactional
    @DeleteMapping("/${id}")
    public void deleteOneById(@PathVariable("id") String id) throws EmptyIdException, EmptyUserIdException, EmptyLoginException {
        @NotNull final UserDTO user = userService.getByLoginDTO("admin");
        projectService.deleteOneById(user.getId(), id);
    }

}
